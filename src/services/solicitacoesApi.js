import api from "@/services/api.js";
import axios from "axios";
const apiSolicitacoes = {
  path: "solicitacao",
  async buscaSolicitacoes(solicitante, concluida, inicio, fim) {
    return await api.kitPost(
      `${this.path}?solicitante=${solicitante}&concluida=${concluida}&inicio=${inicio}&fim=${fim}`,
      { baseURL: localStorage.getItem("api-url") }
    );
  },
  async buscaSolicitacao(id) {
    return await api.kitGet(`${this.path}/${id}`, {
      baseURL: localStorage.getItem("api-url"),
    });
  },
  async buscaSolicitacoesSolicitante(
    dataInicio,
    dataFim,
    idEmpresa,
    status,
    qtdItens,
    pagina
  ) {
    let url = `${this.path}/solicitante`;
    if (dataInicio || dataFim || idEmpresa || status || qtdItens || pagina) {
      url += "?";
      let filtros = [];
      if (dataInicio && typeof dataInicio === "object") {
        filtros.push(`data-inicio=${dataInicio.getTime()}`);
      }
      if (dataFim && typeof dataFim === "object") {
        filtros.push(`data-fim=${dataFim.getTime()}`);
      }
      if (idEmpresa) {
        filtros.push(`empresa=${idEmpresa}`);
      }
      if (status) {
        status.forEach((s) => filtros.push(`status=${s}`));
      }
      if (qtdItens) {
        filtros.push(`qtd-itens=${qtdItens}`);
      }
      if (pagina) {
        filtros.push(`page=${pagina}`);
      }
      for (var i = 0; i < filtros.length; i++) {
        i == 0 ? (url += filtros[i]) : (url += "&" + filtros[i]);
      }
    }
    return await api.kitGet(url, {
      baseURL: localStorage.getItem("api-url"),
    });
  },
  async criaSolicitacaoPorEmpresa(solicitacao) {
    return await api.kitPost(`${this.path}`, solicitacao, {
      baseURL: localStorage.getItem("api-url"),
    });
  },
  async atualizaSolicitacao(solicitacao) {
    return await api.kitPut(`${this.path}`, solicitacao, {
      baseURL: localStorage.getItem("api-url"),
    });
  },
  async excluiSolicitacaoPropria(id) {
    return await api.kitDelete(`${this.path}/minha/${id}`, {
      baseURL: localStorage.getItem("api-url"),
    });
  },
  async excluiSolicitacao(id) {
    return await api.kitDelete(`${this.path}/${id}`, {
      baseURL: localStorage.getItem("api-url"),
    });
  },
  async buscarPesquisasTodas() {
    return await api.kitGet(`${this.path}/todas`, {
      baseURL: localStorage.getItem("api-url"),
    });
  },
  async buscarPesquisasNaoConcluidas() {
    return await api.kitGet(`${this.path}/nao-concluidas`, {
      baseURL: localStorage.getItem("api-url"),
    });
  },
  async buscarPesquisasConcluidas(
    dataInicio,
    dataFim,
    idEmpresa,
    solicitante,
    atendente,
    qtdItens,
    pagina
  ) {
    let url = `${this.path}/concluidas`;
    if (
      dataInicio ||
      dataFim ||
      idEmpresa ||
      solicitante ||
      atendente ||
      qtdItens ||
      pagina
    ) {
      url += "?";
      let filtros = [];
      if (dataInicio && typeof dataInicio === "object") {
        filtros.push(`data-inicio=${dataInicio.getTime()}`);
      }
      if (dataFim && typeof dataFim === "object") {
        filtros.push(`data-fim=${dataFim.getTime()}`);
      }
      if (idEmpresa) {
        filtros.push(`empresa=${idEmpresa}`);
      }
      if (solicitante) {
        filtros.push(`solicitante=${solicitante}`);
      }
      if (atendente) {
        filtros.push(`atendente=${atendente}`);
      }
      if (qtdItens) {
        filtros.push(`qtd-itens=${qtdItens}`);
      }
      if (pagina) {
        filtros.push(`page=${pagina}`);
      }
      for (var i = 0; i < filtros.length; i++) {
        i === 0 ? (url += filtros[i]) : (url += "&" + filtros[i]);
      }
    }
    return await api.kitGet(url, {
      baseURL: localStorage.getItem("api-url"),
    });
  },
  async atender(id) {
    return await api.kitGet(`${this.path}/atender/${id}`, {
      baseURL: localStorage.getItem("api-url"),
    });
  },
  async concluir(id) {
    return await api.kitGet(`${this.path}/concluir/${id}`, {
      baseURL: localStorage.getItem("api-url"),
    });
  },
  async reverter(id) {
    return await api.kitGet(`${this.path}/reverter/${id}`, {
      baseURL: localStorage.getItem("api-url"),
    });
  },
  async buscarSolicitantes() {
    return await api.kitGet(`${this.path}/buscarSolicitantes`, {
      baseURL: localStorage.getItem("api-url"),
    });
  },
  async buscarListaRelatorio(filtro) {
    return await api.kitPost(`${this.path}/listaRelatorio`, filtro, {
      baseURL: localStorage.getItem("api-url"),
    });
  },
  async baixarRelatorio(filtro) {
    return await axios({
      method: "post",
      url: "/solicitacao/relatorio",
      responseType: "blob",
      headers: { Accept: "application/octet-stream" },
      withCredentials: true,
      data: filtro,
      baseURL: localStorage.getItem("api-url"),
    });
  },
};

export default apiSolicitacoes;
