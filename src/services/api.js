import axios from "axios";
import Alerta from "@/utils/Alerta.js";
import Loader from "@/utils/Loader.js";

const Api = axios.create({
  withCredentials: true,
});

Api.all = axios.all;
Api.spread = axios.spread;
Api.interceptors.response.use(
  (config) => {
    //checando versao do back e do front e forçando a dar clear no cache
    let versaoFront = `${process.env.VUE_APP_VERSION}`;
    let versaoBack = config.headers["versao-app"] || "default";
    if (versaoFront !== versaoBack) {
      console.log(`versaoFront:${versaoFront}`);
      console.log(`versaoBack:${versaoBack}`);
      window.location.reload();
    }

    return config;
  },
  (error) => {
    try {
      if (error) {
        if (
          error.response != null &&
          error.response.data != null &&
          error.config != null &&
          error.config.method != null &&
          error.config.url != null
        ) {
          console.log(
            error.config.method,
            error.config.url,
            error.response.status,
            error.response.data
          );
        } else console.log(error);
      } else console.log(error);
    } catch (e) {
      console.log(e);
    }
    return Promise.reject(error);
  }
);

Api.erroPadrao = (error) => {
  let erroMsg = "";
  let stackTrace = "";
  if (error && error.response != null && error.response.data != null) {
    if (error.response.status == 404 || error.response.status == 500) {
      erroMsg = "Serviço está indisponível, tente novamente mais tarde";
    } else if (error.response.status === 777) {
      erroMsg = error.response.data.msg;
      stackTrace = error.response.data.stackTrace;
      Alerta.erroStackTrace(erroMsg, stackTrace);
      return;
    } else {
      erroMsg = `${error.response.status} - ${error.response.data} `;
    }
  } else {
    erroMsg = `Erro desconhecido. Por favor entre em contato com o suporte.`;
  }
  Alerta.erro(erroMsg);
};

Api.kitGet = async (path, config, showError = true, showLoader = true) => {
  if (showLoader) Loader.on();
  return await Api.get(path, config)
    .then((response) => {
      return response;
    })
    .catch((error) => {
      if (showError) Api.erroPadrao(error);
      throw error;
    })
    .finally(() => {
      if (showLoader) Loader.off();
    });
};
Api.kitPost = async (
  path,
  requestObject,
  config,
  showError = true,
  showLoader = true
) => {
  if (showLoader) Loader.on();
  return await Api.post(path, requestObject, config)
    .then((response) => {
      return response;
    })
    .catch((error) => {
      if (showError) Api.erroPadrao(error);
      throw error;
    })
    .finally(() => {
      if (showLoader) Loader.off();
    });
};
Api.kitPut = async (
  path,
  requestObject,
  config,
  showError = true,
  showLoader = true
) => {
  if (showLoader) Loader.on();
  return await Api.put(path, requestObject, config)
    .then((response) => {
      return response;
    })
    .catch((error) => {
      if (showError) Api.erroPadrao(error);
      throw error;
    })
    .finally(() => {
      if (showLoader) Loader.off();
    });
};
Api.kitDelete = async (path, config, showError = true, showLoader = true) => {
  if (showLoader) Loader.on();
  return await Api.delete(path, config)
    .then((response) => {
      return response;
    })
    .catch((error) => {
      if (showError) Api.erroPadrao(error);
      throw error;
    })
    .finally(() => {
      if (showLoader) Loader.off();
    });
};

export async function ping() {
  const ping = axios
    .get(
      `${process.env.VUE_APP_URL_EXT}/${process.env.VUE_APP_PATH}/auth/ping`,
      { withCredentials: true }
    )
    .then(() => {
      localStorage.setItem(
        "api-url",
        `${process.env.VUE_APP_URL_EXT}/${process.env.VUE_APP_PATH}`
      );
    })
    .catch(() => {
      localStorage.setItem(
        "api-url",
        `${process.env.VUE_APP_URL_INT}/${process.env.VUE_APP_PATH}`
      );
    });
  return ping;
}

export default Api;
