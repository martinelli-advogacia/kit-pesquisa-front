import api from "@/services/api.js";
const apiAuth = {
  path: "auth",
  async login(user) {
    return await api.kitPost(`${this.path}/login`, user, {
      baseURL: localStorage.getItem("api-url"),
    });
  },
  async logout() {
    return await api.kitGet(`${this.path}/logout`, {
      baseURL: localStorage.getItem("api-url"),
    });
  },
  async ping() {
    return await api.kitGet(`${this.path}/ping`, {
      baseURL: localStorage.getItem("api-url"),
    });
  },
};

export default apiAuth;
