import api from '@/services/api.js';

const apiEmpresa = {
    path: 'empresa',
    async buscarEmpresa(param){
        return await api.kitGet(`${this.path}/buscar?param=${param}`, {baseURL: localStorage.getItem('api-url')}, true, false);
    }
}

export default apiEmpresa;
