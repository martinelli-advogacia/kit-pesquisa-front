import api from "@/services/api.js";
const apiItemPesquisa = {
  path: "item-pesquisa",
  async excluirSemResultadoItem(listaItens) {
    await api.kitPost(`${this.path}/sem-resultado?flag=false`, listaItens, {baseURL: localStorage.getItem('api-url')});
  },
  async inserirSemResultadoItem(listaItens) {
    await api.kitPost(`${this.path}/sem-resultado`, listaItens, {baseURL: localStorage.getItem('api-url')});
  },
};

export default apiItemPesquisa;
