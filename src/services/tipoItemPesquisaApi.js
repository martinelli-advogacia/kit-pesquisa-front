import api from "@/services/api.js";
const apiTipoItemPesquisa = {
  path: "tipo-item",
  async buscaTiposItens() {
    return await api.kitGet(`${this.path}`, {baseURL: localStorage.getItem('api-url')});
  },
  async buscarSemArquivo(id) {
    return await api.kitGet(`${this.path}/semArquivo/${id}`, {baseURL: localStorage.getItem('api-url')});
  },
};

export default apiTipoItemPesquisa;
