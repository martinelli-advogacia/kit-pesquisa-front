import api from "@/services/api.js";
const apiUsuario = {
    path: "usuario",
    async buscaUsuarios(){
        return await api.kitGet(`${this.path}`, {baseURL: localStorage.getItem('api-url')});
    },
    async buscaUsuario(login){
        return await api.kitGet(`${this.path}/${login}`, {baseURL: localStorage.getItem('api-url')});
    },
    async buscaUsuarioAD(nome){
        return await api.kitGet(`${this.path}/ad?nome=${nome}`, {baseURL: localStorage.getItem('api-url')}, true, false);
    },
    async buscarSolicitantes(busca){
        return await api.kitGet(`${this.path}/buscar?param=${busca}`, {baseURL: localStorage.getItem('api-url')}, true, false);
    },
    async buscarAtendentes(){
        return await api.kitGet(`${this.path}/atendentes`, {baseURL: localStorage.getItem('api-url')}, true, false);
    },
    async updateUsuario(usuario){
        return await api.kitPut(`${this.path}`, usuario, {baseURL: localStorage.getItem('api-url')});
    },
    async updateUfUsuario(usuario){
        return await api.kitPut(`${this.path}/uf`, usuario, {baseURL: localStorage.getItem('api-url')});
    }
};

export default apiUsuario;
