import api from "@/services/api.js";
const apiArquivo = {
  path: "arquivo",
  async salvarArquivo(idSolicitacao, formData) {
    return await api.kitPost(`${this.path}/${idSolicitacao}`, formData, {
      baseURL: localStorage.getItem("api-url"),
    });
  },
  async deletar(idSolicitacao, idArquivo) {
    return await api.kitDelete(`${this.path}/${idSolicitacao}/${idArquivo}`, {
      baseURL: localStorage.getItem("api-url"),
    });
  },
  async baixar(idSolicitacao, idArquivo) {
    return await api.kitGet(
      `${this.path}/baixar?idSolicitacao=${idSolicitacao}&idArquivo=${idArquivo}`,
      { baseURL: localStorage.getItem("api-url") }
    );
  },
  async baixarTodos(idSolicitacao) {
    return await api.kitGet(`${this.path}/baixarTodos/${idSolicitacao}`, {
      baseURL: localStorage.getItem("api-url"),
    });
  },
  async buscarArquivosSolicitacao(idSolicitacao) {
    return await api.kitGet(`${this.path}/${idSolicitacao}`, {
      baseURL: localStorage.getItem("api-url"),
    });
  },
};

export default apiArquivo;
