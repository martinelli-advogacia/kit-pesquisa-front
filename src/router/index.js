import Vue from "vue";
import VueRouter from "vue-router";
import Login from "@/views/Login";
import CriarPesquisa from "@/views/CriarPesquisa";
import MinhasPesquisas from "@/views/MinhasPesquisas";
import Historico from "@/views/Historico";
import Consultas from "@/views/Pesquisador/Consultas";
import DetalhesConsulta from "@/views/Consultor/DetalhesConsulta";
import DetalhesPesquisa from "@/views/Pesquisador/DetalhesPesquisa";
import Relatorios from "@/views/Administrador/Relatorios";
import AdmPerfil from "@/views/Administrador/AdmPerfil";
import Erro from "@/views/Erro";
import Health from "@/views/Health";
import ApiAuth from "@/services/authApi.js";

Vue.use(VueRouter);

function verificarAutenticacao(to, from, next) {
  if (!localStorage.getItem("user")) {
    router.push({ name: "Login" });
  } else {
    next();
  }
}

function verificaPermissaoPesquisador(to, from, next) {
  if (!localStorage.getItem("user")) {
    router.push({ name: "Login" });
  }
  if (
    localStorage.getItem("rolesAllowed") === "Pesquisador" ||
    localStorage.getItem("rolesAllowed") === "Administrador"
  ) {
    next();
  } else {
    router.push({ name: "Login" });
  }
}

function verificaPermissaoAdministrador(to, from, next) {
  if (!localStorage.getItem("user")) {
    router.push({ name: "Login" });
  }
  if (localStorage.getItem("rolesAllowed") === "Administrador") {
    next();
  } else {
    router.push({ name: "Login" });
  }
}

const routes = [
  {
    path: "/",
    name: "Login",
    component: Login,
    beforeEnter: async (to, from, next) => {
      localStorage.removeItem("user");
      localStorage.removeItem("rolesAllowed");
      await ApiAuth.logout();
      next();
    },
  },
  {
    path: "/criar-pesquisa",
    name: "Criar Pesquisa",
    beforeEnter: verificarAutenticacao,
    component: CriarPesquisa,
  },
  {
    path: "/minhas-pesquisas",
    name: "Minhas Pesquisas",
    beforeEnter: verificarAutenticacao,
    component: MinhasPesquisas,
  },
  {
    path: "/historico",
    name: "Historico",
    beforeEnter: verificarAutenticacao,
    component: Historico,
  },
  {
    path: "/consultas",
    beforeEnter: verificaPermissaoPesquisador,
    name: "Consultas",
    component: Consultas,
  },
  {
    path: "/detalhes-pesquisa/:idSolicitacao/:tab",
    props: true,
    name: "Detalhes Pesquisa",
    beforeEnter: verificaPermissaoPesquisador,
    component: DetalhesPesquisa,
  },
  {
    path: "/detalhes-consulta/:idSolicitacao",
    props: true,
    name: "Detalhes Consulta",
    beforeEnter: verificarAutenticacao,
    component: DetalhesConsulta,
  },
  {
    path: "/adm-perfil",
    props: true,
    name: "adm-perfil",
    beforeEnter: verificaPermissaoAdministrador,
    component: AdmPerfil,
  },
  {
    path: "/relatorios",
    name: "Relatorios",
    component: Relatorios,
    beforeEnter: verificaPermissaoAdministrador,
  },
  {
    path: "/erro",
    name: "Erro",
    component: Erro,
  },
  {
    path: "/health",
    name: "Health",
    component: Health,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
