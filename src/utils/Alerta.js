var Alerta = {
  vm: document.querySelector("#app").__vue__,
  getVm() {
    this.vm = document.querySelector("#app").__vue__;
  },
  sucesso(texto) {
    if (this.vm == null) this.getVm();
    if (this.vm != null)
      this.vm.$swal({
        title: "Sucesso!",
        html: texto,
        icon: "success",
        showConfirmButton: true,
        width: 800,
      });
  },
  sucessoConfirm(callback, texto) {
    if (this.vm == null) this.getVm();
    if (this.vm != null)
      this.vm.$swal
        .fire({
          title: "Sucesso!",
          text: texto,
          icon: "success",
          confirmButtonColor: "#3085d6",
          confirmButtonText: "Voltar",
          width: 800,
        })
        .then((result) => {
          callback(result.isConfirmed);
        });
  },
  erro(texto) {
    if (this.vm == null) this.getVm();
    if (this.vm != null)
      this.vm.$swal({
        title: "Erro!",
        text: texto,
        icon: "error",
        showConfirmButton: true,
        width: 800,
      });
  },
  erroConfirm(callback, texto) {
    if (this.vm == null) this.getVm();
    if (this.vm != null)
      this.vm.$swal
        .fire({
          title: "Erro!",
          text: texto,
          icon: "error",
          showConfirmButton: true,
          confirmButtonText: "Voltar",
          width: 800,
        })
        .then((result) => {
          callback(result.isConfirmed);
        });
  },
  warningConfirm(callback, texto) {
    if (this.vm == null) this.getVm();
    if (this.vm != null)
      this.vm.$swal
        .fire({
          title: "Atenção!",
          text: texto,
          icon: "warning",
          showCancelButton: true,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: "Sim",
          cancelButtonText: "Cancelar",
          width: 800,
        })
        .then((result) => {
          callback(result.isConfirmed);
        });
  },
  erroStackTrace(texto, stackTrace) {
    if (this.vm == null) this.getVm();
    if (this.vm != null)
      this.vm.$swal({
        title: "Erro!",
        icon: "error",
        html: `${texto} <textarea style="width:100%" rows="10" onclick="this.focus();this.select()" readonly>${stackTrace}</textarea>`,
        showConfirmButton: true,
      });
  },
  customConfirm(callback, title, text) {
    if (this.vm == null) this.getVm();
    if (this.vm != null)
      this.vm.$swal
        .fire({
          title: title,
          text: text,
          showCancelButton: true,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: "Sim",
          cancelButtonText: "Não",
          width: 800,
        })
        .then((result) => {
          callback(result.isConfirmed);
        });
  },
};
export default Alerta;
