var Loader = {
  vm: document.querySelector("#app").__vue__,
  getVm() {
    this.vm = document.querySelector("#app").__vue__;
  },
  on() {
    if (this.vm == null) this.getVm();
    if (this.vm != null) this.vm.$root.$emit("loader-on");
  },
  off() {
    if (this.vm == null) this.getVm();
    if (this.vm != null) this.vm.$root.$emit("loader-off");
  },
};
export default Loader;
