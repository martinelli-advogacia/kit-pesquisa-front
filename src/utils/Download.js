var Download = {
  getBase64(response) {
    const anexo = response.data;
    const link = document.createElement("a");
    link.href = `data:${anexo.contentType};base64,${anexo.anexo}`;
    link.setAttribute("download", anexo.fileName);
    document.body.appendChild(link);
    link.click();
  },
  getXLSX(response, filename) {
    const url = window.URL.createObjectURL(new Blob([response.data]));
    const link = document.createElement("a");
    link.href = url;
    link.setAttribute("download", filename + ".xlsx");
    document.body.appendChild(link);
    link.click();
  },
};
export default Download;
